conda activate kaggle

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
cp kernelgpu/kernel-metadata.json.almazseo kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/


export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazurov
cp kernelgpu/kernel-metadata.json.almazurov kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kagglekagkagdevprod
cp kernelgpu/kernel-metadata.json.kagkagdevprod kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/

export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp kernelgpu/kernel-metadata.json.remotejob kagglegenerate/kernel-metadata.json
kaggle kernels push -p kagglegenerate/